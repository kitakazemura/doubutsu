#include <math.h>
#include <stdio.h>

enum pieceTypeE {
    LION,
    GIRAFFLE,
    ELEPHANT,
    CHICK,
    EMP,  // EMPは、空駒定義として(暫定的に)使用している。
    HEN
};
typedef struct {
    int direction;  //{-1 or 0 or 1}
    char user[256];
} player;
typedef struct {
    int x;
    int y;
    player *dealer;
    char piceString;  //何に使うん？
    enum pieceTypeE type;
} komadata;
typedef struct {
    komadata *banmenp[4][3];
    komadata *mochigoma[6];
} board;

void dispmochigoma(komadata **mochigoma, int playerInt, player **plp) {
    // playerIntは1が先手、-1が後手
    void mochigomaclear(mochigoma);
    for (int i = 0; i < 6; i++) {
        if (playerInt == 1) {
            //先手の駒を表示
            if (&plp[0] == mochigoma[i]->dealer) {
                printf("%d の駒は先手の持ち駒\n", i);
            }
        } else {
            if (&plp[1] == mochigoma[i]->dealer) {
                printf("%d の駒は後手の持ち駒\n", i);
            }
        }
        /*
        mochigoma->x;

        if (mochigoma = 1) {
            printf("先手持ち駒▼ %p\n", mochigoma);
        } else {
            printf("後手持ち駒▽ %p\n", mochigoma);
        }*/
    }
}

void mochigomaclear(komadata **mochigoma) {
    int i;
    for (i = 0; i < 6; i++) {
        mochigoma[i] = NULL;
    }
}

int main() {
    //
    return 0;
}