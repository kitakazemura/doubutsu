typedef struct{
    char *user1;
    char *user2;
}player;

typedef struct{
    int x;
    int y;
    player *dealer;
    char peceString[10];
    pieceTypeE type;
}komadata;

typedef struct{
    int didChangeDealer;
    komadata *target;
    int didNari;
    int x0;
    int y0;
    int x1;
    int y1;
}rireki;

typedef struct{
    komadata *komap[3][4];
}board;

typedef struct{//可変調になるものを構造体に入れてはダメだ！
    int direction;
    char *user;
}player[2];

void rireki(int x0, int y0, int x1, int y1, komadata *target, int ChangeDealer, int didNari);

//komadataに付随する関数
int AffiliationChange(int a);//コマの所属先変更(とった、とられたを判断)
int MoveTop(int in);//コマを動かす関数(dx, dyを置き換える)関数。変数inはキーボード入力
void promoteToHen(board *bp, piece, *pip);//ひよこから鳥になる関数
void moveKoma(int *moveData);//鳥からひよこになる関数
void moveKoma(int *moveData);//コマをどのように動かすかを判断する関数

//boardに付随する関数
int isOntoEnemy(komadata *koma, board *brd, int x, int y);//動かしたコマが敵陣の駒と重なったかどうかを判断する関数
int isOntoLion(komadata *koma, int x, int y);


