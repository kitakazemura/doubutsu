#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "doubutsu.h"

int main() {
    komadata cp[8];
    board d;
    player pl[2];
    rireki inrireki[2];
    char a[256], b[256];  //プレイヤー名の格納

    init(pl, cp, &d, inrireki, a, b);
    display(pl, &d, inrireki);
    return 0;
}