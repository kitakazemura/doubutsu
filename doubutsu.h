#ifndef __DOUBUTSU_H__
#define __DOUBUTSU_H__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum pieceTypeE { LION, GIRAFFLE, ELEPHANT, CHICK, HEN };

typedef struct {
    int direction;  //{-1 or 1}
    char *user;
} player;

typedef struct {
    int x;
    int y;
    player *dealer;
    char *piceString;  //"ヒ"や"ラ"などを入れておく
    enum pieceTypeE type;
} komadata;

typedef struct {
    komadata *banmenp[4][3];
    komadata *mochigoma[6];
} board;

typedef struct {
    int didChangeDealer;
    komadata *target;
    int didNari;
    int x0;
    int y0;
    int x1;
    int y1;
} rireki;

//ここから関数

void setPlayer(player *plp, char *a, char *b) {
    plp[0].direction = 1;
    printf("ユーザ名を入力してください\n");
    scanf("%s", a);
    plp[0].user = a;
    printf("ようこそ%sさん\n\n", plp[0].user);
    printf("ユーザ名を入力してください\n");
    plp[1].direction = -1;
    scanf("%s", b);
    plp[1].user = b;
    printf("ようこそ%sさん\n\n\n", plp[1].user);
}

void setKomadata(komadata *cp, player *plp) {
    // komadata[4(y)][3(x)]型に、初期駒情報(空駒を含む)をそれぞれ代入する関数。ここを変更すると下に記載しているprintBoardに影響する。
    cp->y = 0;
    cp->dealer = &plp[1];
    cp->piceString = "キ";
    cp->type = GIRAFFLE;
    cp++;

    cp->x = 1;
    cp->y = 0;
    cp->dealer = &plp[1];
    cp->piceString = "ラ";
    cp->type = LION;
    cp++;

    cp->x = 2;
    cp->y = 0;
    cp->dealer = &plp[1];
    cp->piceString = "ゾ";
    cp->type = ELEPHANT;
    cp++;

    cp->x = 1;
    cp->y = 1;
    cp->dealer = &plp[1];
    cp->piceString = "チ";
    cp->type = CHICK;  //
    cp++;

    cp->x = 1;
    cp->y = 2;
    cp->dealer = &plp[0];
    cp->piceString = "チ";
    cp->type = CHICK;  //
    cp++;

    cp->x = 0;
    cp->y = 3;
    cp->dealer = &plp[0];
    cp->piceString = "ゾ";
    cp->type = ELEPHANT;
    cp++;

    cp->x = 1;
    cp->y = 3;
    cp->dealer = &plp[0];
    cp->piceString = "ラ";
    cp->type = LION;
    cp++;

    cp->x = 2;
    cp->y = 3;
    cp->dealer = &plp[0];
    cp->piceString = "ゾ";
    cp->type = GIRAFFLE;
}

void initbrd(komadata *cp, board *brd) {
    // komadata型変数とboard型変数を引数として渡すことで、board型変数を返す(返り値ではないが)関数。
    brd->banmenp[0][0] = &cp[0];
    brd->banmenp[0][1] = &cp[1];
    brd->banmenp[0][2] = &cp[2];
    brd->banmenp[1][0] = NULL;  //空きコマについてはNULLを入れることで対処。
    brd->banmenp[1][1] = &cp[3];
    brd->banmenp[1][2] = NULL;
    brd->banmenp[2][0] = NULL;
    brd->banmenp[2][1] = &cp[4];
    brd->banmenp[2][2] = NULL;
    brd->banmenp[3][0] = &cp[5];
    brd->banmenp[3][1] = &cp[6];
    brd->banmenp[3][2] = &cp[7];
}

char *setPieceName(enum pieceTypeE type) {
    //格納されているpieceTypeEを元に、表示の為の動物名をchar型に格納する関数。main文中では使わない。
    char *a = NULL;
    switch (type) {
        case LION:
            a = "ラ";
            break;

        case GIRAFFLE:
            a = "キ";
            break;

        case ELEPHANT:
            a = "ゾ";
            break;

        case CHICK:
            a = "ヒ";
            break;

        case HEN:
            a = "二";
            break;
    }
    return a;
}

char *setDirection(int direction) {
    //格納されているdirection情報を元に、表示の為の方向情報(△
    // or ▽)をchar型に格納する関数。main文中では使わない。
    char *cp = NULL;
    cp = (char *)malloc(
        sizeof(char) *
        4);  //動的メモリを確保する関数。文字列を返り値にする場合はほぼmast!!
    switch (direction) {
        case -1:
            cp = "▼";
            break;

        case 1:
            cp = "△";
            break;

        case 0:
            cp = "  ";
            break;
    }
    return cp;
}

void dispKomadata(komadata *komap) {
    if (komap == NULL) {
        // OS間の文字サイズの差異の吸収
#if defined(_WIN32) || defined(_WIN64)
        printf(" -- ");
#else
        printf(" - ");
#endif

    } else {
        int dir = komap->dealer->direction;
        // printf("%d\n\n%p\n\n", dir,komap->dealer);
        printf("%s%s", setDirection(dir), setPieceName(komap->type));
    }
}

void printBoard(board *brd) {
    // board型変数を引数として渡すことで、(現時点での)盤面の表示が可能になる。
    // OS間の文字サイズの差異の吸収
#if defined(_WIN32) || defined(_WIN64)
    printf(" | A  | B  | C  |\n");
#else
    printf(" | A | B | C |\n");
#endif
    for (int i = 0; i < 9; i++) {
        if ((i % 2) == 0) {
#if defined(_WIN32) || defined(_WIN64)
            printf("-+----+----+----+\n");
#else
            printf("-+---+---+---+\n");
#endif

        } else {
            printf("%d|", (int)i - i / 2);
            for (int j = 0; j < 3; j++) {
                dispKomadata(brd->banmenp[(int)i / 2][j]);
                printf("|");
            }
            printf("\n");
        }
    }
}

void rirekiclear(rireki *rirekip) {
    int i = 0;
    for (i = 0; i < 2; i++) {
        rirekip[i].didChangeDealer = -1;
        rirekip[i].target = NULL;
        rirekip[i].x0 = -1;
        rirekip[i].y0 = -1;
        rirekip[i].x1 = -1;
        rirekip[i].y1 = -1;
    }
}

void disprireki(rireki *rirekip) {
    int i;
    for (i = 0; i < 2; i++) {
        if (rirekip[i].target == NULL) {
            printf("%d手前の履歴がありません\n", i + 1);
        } else {
            printf("%d手前 %d%d%d\n", i + 1,
                   (rirekip[i].target)->dealer->direction, rirekip[i].x1,
                   rirekip[i].y1);
        }
    }
}

void mochigomaclear(komadata **mochigoma) {
    int i;
    for (i = 0; i < 6; i++) {
        mochigoma[i] = NULL;
    }
}

void dispmochigoma(komadata **mochigoma, int playerInt, player *plp) {
    // playerIntは1が先手、-1が後手
    // mochigomaclear(mochigoma);
    for (int i = 0; i < 6; i++) {
        if (mochigoma[i] == NULL) {
        } else {
            if (playerInt == 1) {
                //先手の駒を表示
                if (&plp[0] == mochigoma[i]->dealer) {
                    // printf("%d の駒は先手の持ち駒\n", i);
                    dispKomadata(mochigoma[i]);
                    printf("\t");
                }
            } else {
                if (&plp[1] == mochigoma[i]->dealer) {
                    // printf("%d の駒は後手の持ち駒\n", i);
                    dispKomadata(mochigoma[i]);
                    printf("\t");
                }
            }
        }
    }
}

void init(player *plp, komadata *cp, board *brd, rireki *rirekip, char *a,
          char *b) {
    setPlayer(plp, a, b);
    setKomadata(cp, plp);  // komadata型(1次元)配列を渡して初期化。
    initbrd(cp, brd);  //初期化した配列とboard型を渡してboardを初期化。
    rirekiclear(rirekip);
    mochigomaclear(brd->mochigoma);
}

void display(player *plp, board *brd, rireki *rirekip) {
    printf("%s(後手)の持駒:", plp[1].user);
    dispmochigoma(brd->mochigoma, -1, plp);
    printf("\n");
    printBoard(brd);  //初期化したboardを表示させる。
    printf("%s(先手)の持駒:", plp[0].user);
    dispmochigoma(brd->mochigoma, 1, plp);
    printf("\n");
    mochigomaclear(brd->mochigoma);
    disprireki(rirekip);
}
#endif