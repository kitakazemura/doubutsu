#include <math.h>
#include <stdio.h>
/*
rireki inrireki[2];
rirekip = &inrireki;
void rirekiclear(rireki *rirekip) {
    rirekip[0].didChangeDealer = -1;
    rirekip[0].*target = NULL;
    rirekip[0].x0 = -1;
    rirekip[0].y0 = -1;
    rirekip[0].x1 = -1;
    rirekip[0].y1 = -1;
    rirekip[0].didNari = -1;
    rirekip[1].didChangeDealer = -1;
    rirekip[1].*target = NULL;
    rirekip[1].x0 = -1;
    rirekip[1].y0 = -1;
    rirekip[1].x1 = -1;
    rirekip[1].y1 = -1;
    rirekip[1].didNari = -1;
}*/
typedef struct{
    int direction;//{-1 or 0 or 1}
    char user[256];
}player;

enum pieceTypeE{
    LION,
    GIRAFFLE,
    ELEPHANT,
    CHICK,
    EMP, //EMPは、空駒定義として(暫定的に)使用している。
    HEN
};


typedef struct{
    int x;
    int y;
    player dealer;
    char piceString[10];//何に使うん？
    enum pieceTypeE type;
}komadata;

typedef struct{
    int didChangeDealer;
    komadata *target;
    int didNari;
    int x0;
    int y0;
    int x1;
    int y1;
}rireki;

typedef struct{
    komadata *komap[3][4];
}board;







void rirekiclear(rireki *rirekip) {
    int i = 0;
    for (i = 0; i < 2; i++) {
        rirekip[i].didChangeDealer = -1;
        rirekip[i].target = NULL;
        rirekip[i].x0 = -1;
        rirekip[i].y0 = -1;
        rirekip[i].x1 = -1;
        rirekip[i].y1 = -1;
    }
}

void disprireki(rireki *rirekip) {
    int i;
    for(i=0;i<2;i++){
        if(rirekip[i].target==NULL){
            printf("%d手前の履歴がありません\n",i+1);
        }else{
            printf("%d手前 %d%d%d\n",i+1,rirekip[i].target->dealer.direction,rirekip[i].x1,rirekip[i].y1);
        }
    } 
}

int main() {
    rireki inrireki[2];
    rireki *rirekip[2] = {&inrireki[0],&inrireki[1]};
    rirekiclear(*rirekip);
    disprireki(*rirekip);
    return 0;
}