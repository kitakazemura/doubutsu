#include <stdio.h>
#include <string.h>
#include <stdlib.h>

enum pieceTypeE{
    LION,
    GIRAFFLE,
    ELEPHANT,
    CHICK,
    EMP, //EMPは、空駒定義として(暫定的に)使用している。
    HEN
};

typedef struct{
    int direction;//{-1 or 1}
    char *user;
}player;
 

typedef struct{
    int x;
    int y;
    player *dealer;
    char *piceString; //"ヒ"や"ラ"などを入れておく
    enum pieceTypeE type;
}komadata;


typedef struct{
    komadata *banmenp[4][3];
    komadata *mochigoma[6];
}board;

void setPlayer(player *plp);
void setKomadata(komadata *cp,player *plp);
void initbrd(komadata *cp, board *brd);
char *setPieceName(enum pieceTypeE type);
char *setDirection(int direction);
void printBoard(board *brd);
void dispKomadata(komadata *komap);


int main(){
    
    komadata cp[8];
    board d;
    player pl[2];
    
    setPlayer(pl);
    setKomadata(cp,pl);//komadata型(1次元)配列を渡して初期化。
    initbrd(cp, &d);//初期化した配列とboard型を渡してboardを初期化。
    printBoard(&d);//初期化したboardを表示させる。
    
    return 0;
}

void setPlayer(player *plp){
    plp[0].direction = 1;
    plp[0].user = "NAOTO\0";
    plp[1].direction = -1;
    plp[1].user = "KEN\0";
}




void setKomadata(komadata *cp,player *plp){
    //komadata[4(y)][3(x)]型に、初期駒情報(空駒を含む)をそれぞれ代入する関数。ここを変更すると下に記載しているprintBoardに影響する。
    cp->y = 0;
    cp->dealer=&plp[1];
    cp->piceString="キ";
    cp->type = GIRAFFLE;
    cp++;
    
    cp->x = 1;
    cp->y = 0;
    cp->dealer=&plp[1];
    cp->piceString="ラ";
    cp->type = LION;
    cp++;
    
    cp->x = 2;
    cp->y = 0;
    cp->dealer=&plp[1];
    cp->piceString="ゾ";
    cp->type = ELEPHANT;
    cp++;
    
    cp->x = 1;
    cp->y = 1;
    cp->dealer=&plp[1];
    cp->piceString="チ";
    cp->type = CHICK;//
    cp++;

    cp->x = 1;
    cp->y = 2;
    cp->dealer=&plp[0];
    cp->piceString="チ";
    cp->type = CHICK;//
    cp++;
    
    cp->x = 0;
    cp->y = 3;
    cp->dealer=&plp[0];
    cp->piceString="ゾ";
    cp->type = ELEPHANT;
    cp++;
    
    cp->x = 1;
    cp->y = 3;
    cp->dealer=&plp[0];
    cp->piceString="ラ";
    cp->type = LION;
    cp++;

    cp->x = 2;
    cp->y = 3;
    cp->dealer=&plp[0];
    cp->piceString="ゾ";
    cp->type = GIRAFFLE;
     
   
}

 void initbrd(komadata *cp, board *brd){//komadata型変数とboard型変数を引数として渡すことで、board型変数を返す(返り値ではないが)関数。
    brd->banmenp[0][0] = &cp[0];
    brd->banmenp[0][1] = &cp[1];
    brd->banmenp[0][2] = &cp[2];
    brd->banmenp[1][0] = NULL;//空きコマについてはNULLを入れることで対処。
    brd->banmenp[1][1] = &cp[3];
    brd->banmenp[1][2] = NULL;
    brd->banmenp[2][0] = NULL;
    brd->banmenp[2][1] = &cp[4];
    brd->banmenp[2][2] = NULL;
    brd->banmenp[3][0] = &cp[5];
    brd->banmenp[3][1] = &cp[6];
    brd->banmenp[3][2] = &cp[7];
}

void printBoard(board *brd){//board型変数を引数として渡すことで、(現時点での)盤面の表示が可能になる。
   
    printf(" | A | B | C |\n");
    for(int i=0; i<9; i++){
        if((i%2)==0){
            printf("-+---+---+---+\n");
        }else{
            printf("%d|",(int)i-i/2);
            for(int j=0;j<3;j++){
                dispKomadata(brd->banmenp[(int)i/2][j]);
                printf("|");
            }
            printf("\n");

        }
    }
 
}

void dispKomadata(komadata *komap){
    if(komap==NULL){
        printf(" - ");
    }else{
    int dir = komap->dealer->direction;
        //printf("%d\n\n%p\n\n", dir,komap->dealer);
    printf("%s%s",setDirection(dir), setPieceName(komap->type));
    }

}

//NULLとコマ入りを判断する関数。

char *setPieceName(enum pieceTypeE type){//格納されているpieceTypeEを元に、表示の為の動物名をchar型に格納する関数。main文中では使わない。
    
    char *a = NULL;
    
    switch(type){
        case LION:
            a = "ラ";
            break;
    
        case GIRAFFLE:
            a = "キ";
            break;
        
        case ELEPHANT:
            a = "ゾ";
            break;
        
        case CHICK:
            a = "ヒ";
            break;
            
        case HEN:
            a = "二";
            break;
        
        case EMP:
            a = "- ";
            break;
    }
        return a;
}

char *setDirection(int direction){//格納されているdirection情報を元に、表示の為の方向情報(△ or ▽)をchar型に格納する関数。main文中では使わない。
    
    char *cp = NULL;
    
    cp = (char*)malloc(sizeof(char) * 4);//動的メモリを確保する関数。文字列を返り値にする場合はほぼmast!!
    
    
    switch(direction){
        case -1:
            cp = "▽";
            break;
        
        case 1:
            cp = "△";
            break;
            
        case 0:
            cp = " ";
            break;
    }
    return cp;
}



